<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TranslatorTasksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TranslatorTasksTable Test Case
 */
class TranslatorTasksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TranslatorTasksTable
     */
    public $TranslatorTasks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TranslatorTasks',
        'app.Languages',
        'app.Translators',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TranslatorTasks') ? [] : ['className' => TranslatorTasksTable::class];
        $this->TranslatorTasks = TableRegistry::getTableLocator()->get('TranslatorTasks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TranslatorTasks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
