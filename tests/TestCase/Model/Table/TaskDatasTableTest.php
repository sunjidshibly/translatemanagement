<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaskDatasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaskDatasTable Test Case
 */
class TaskDatasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TaskDatasTable
     */
    public $TaskDatas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TaskDatas',
        'app.TranslatorTasks',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TaskDatas') ? [] : ['className' => TaskDatasTable::class];
        $this->TaskDatas = TableRegistry::getTableLocator()->get('TaskDatas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaskDatas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
