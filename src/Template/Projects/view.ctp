<h3><?= h($project->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row">Created By</th>
            <td><?= $project->has('user') ? $project->user->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($project->created) ?></td>
        </tr>
    </table>
