<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= __('Add Project') ?></h1>
            </div>
            <?= $this->Form->create($project,['class'=>'user']) ?>
            <div class="form-group">
                <input type="text" name="title" value="<?= $project->title ?>" class="form-control form-control-user" id="title" placeholder="Project Title">
            </div>
            <?= $this->Form->button('Add Project',['class'=>'btn btn-primary btn-user btn-block']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>