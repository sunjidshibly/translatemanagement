<h3><?= h($language->language) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $language->has('user') ? $language->user->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($language->created) ?></td>
        </tr>
    </table>
