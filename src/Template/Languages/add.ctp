<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= __('Add Language') ?></h1>
            </div>
            <?= $this->Form->create($language,['class'=>'user']) ?>
            <div class="form-group">
                <input type="text" name="language" value="<?= $language->language ?>" class="form-control form-control-user" id="language" placeholder="Language Name">
            </div>
            <?= $this->Form->button('Add Language',['class'=>'btn btn-primary btn-user btn-block']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>