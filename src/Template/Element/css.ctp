<!-- Custom fonts for this template-->
<link href="<?= $this->request->getAttribute("webroot") ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="<?= $this->request->getAttribute("webroot") ?>css/sb-admin-2.css" rel="stylesheet">
<link href="<?= $this->request->getAttribute("webroot") ?>css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"/>