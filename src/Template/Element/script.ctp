<!-- Bootstrap core JavaScript-->
<script src="<?= $this->request->getAttribute("webroot") ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= $this->request->getAttribute("webroot") ?>vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="<?= $this->request->getAttribute("webroot") ?>js/sb-admin-2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    function isEmpty(value){
        if(typeof(value) == 'undefined' || value == '' || value == null ||  value == 0){
            return true;
        }
        return false;
    }
</script>