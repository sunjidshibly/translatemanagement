<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"/><style>
    .message{
        word-wrap: break-word;
    }
    .error-message{
        color: red;
    }
</style>
<?php
if(isset($message) && is_array($message)):
    ?>
    <div class="alert alert-danger message error">
        <button class="close" data-close="alert"></button>
        <?php
        foreach ($message as $msg) {
            if (is_array($msg)) {
                foreach ($msg as $key => $error) {
                    echo '<i class="fa fa-circle-o text-red"></i><span>'.$error.'</span>';
                }
            }else{
                echo '<i class="fa fa-circle-o text-red"></i><span>'.$msg.'</span>';
            }
        }
        ?>
    </div>
<?php
endif;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right"
        };
        <?php
        if(isset($message) && !is_array($message)):
        ?>
        toastr.error('<?=$message?>');
        <?php
        endif;
        ?>
</script>
