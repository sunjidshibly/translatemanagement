<?php
if (!empty($params['class'])) {
    $class =$params['class'];
} else{
    $class = 'info';

}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<link rel="stylesheet" type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"/><style>
    .message{
        word-wrap: break-word;
    }
    .error-message{
        color: red;
    }
</style>

<!--<div class="alert alert---><?//= h($class) ?><!-- alert-dismissible">-->
<!--    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
<!--    --><?//= $message ?>
<!--</div>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right"
    };
    toastr.<?= $class ?>('<?php echo h($message) ?>');
</script>
