<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Task Data</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col" style="width: 40%">Source Data</th>
                    <th scope="col" style="width: 40%">Translated Data</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($assinged_task_data as $data): ?>
                    <tr>
                        <td><?php $row_data = json_decode($data->source_data,true);
                            foreach ($row_data as $index=>$coloumn_data): ?>
                                <div class="card shadow">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Row <?= $index; ?></h6>
                                    </div>
                                    <div class="card-body">
                                        <?= $coloumn_data ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </td>
                        <td><?php $row_data = json_decode($data->translated_data,true);
                            foreach ($row_data as $index=>$coloumn_data): ?>
                                <div class="card shadow">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Row <?= $index; ?></h6>
                                    </div>
                                    <div class="card-body card-body-editable" id="<?= $index ?>">
                                        <?= $coloumn_data ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="row">
                <div class="col-4 offset-5">
                    <?= $this->Html->link(
                        'Reset',
                        'tasks/resetTaskData/'.$data->id,
                        ['class' => 'btn btn-warning btn-user']
                    ); ?>
                    <button class="btn btn-primary btn-user btn-save-data" data-t-d-id="<?= $data->id ?>" type="button">Save Data</button>
                    <button class="btn btn-success btn-user btn-next-data" data-t-d-id="<?= $data->id ?>" type="button">Save and next</button>
                </div>
            </div>
            <?php endforeach; ?>
            <?= $this->Form->create(null,['id'=>'saveForm']) ?>
            <?= $this->Form->hidden('translated_data',['id'=>'translated_data']); ?>
            <?= $this->Form->end() ?>
            <hr>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.tiny.cloud/1/r215oghm27y5il6cc9y0z89cggdyblwybx1wgcr67atbpieu/tinymce/5/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: '.card-body-editable',
        plugins: "code"
    });
</script>
<script>
    var js_wb_root = "<?= $this->request->getAttribute('webroot') ?>";
    $(document).on('click', '.btn-save-data', function () {
        var task_data_id = parseInt($(this).data('t-d-id'));
        if (!isEmpty(task_data_id)) {
            setHtmlData();
            $("#saveForm").attr('action',js_wb_root+'tasks/saveTaskData/'+task_data_id);
            $("#saveForm").submit();
        }
    });
    $(document).on('click', '.btn-next-data', function () {
        var task_data_id = parseInt($(this).data('t-d-id'));
        if (!isEmpty(task_data_id)) {
            setHtmlData();
            $("#saveForm").attr('action',js_wb_root+'tasks/saveAndNextTaskData/'+task_data_id);
            $("#saveForm").submit();
        }
    });
    function setHtmlData() {
        var data ={};
        $(".card-body-editable").each(function() {
            var id = $(this).attr('id');
            var html = tinyMCE.get(id).getContent();
            data[id] = html;
        });
        $("#translated_data").val(JSON.stringify(data));
    }
</script>

