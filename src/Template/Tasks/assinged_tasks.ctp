<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Assisnged Tasks</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col">Sl No</th>
                    <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('task_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('translated_language') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('task_total_character') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('translator_id') ?></th>
                    <th scope="col">Status</th>
                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                    <th scope="col">Actions</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 1;
                foreach ($assinged_tasks as $assinged_task): ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $assinged_task->has('project') ? $this->Html->link($assinged_task->project->title, ['controller' => 'Projects', 'action' => 'view', $assinged_task->project->id]) : '' ?></td>
                        <td><?= $assinged_task->has('task') ? $this->Html->link($assinged_task->task->title, ['controller' => 'Tasks', 'action' => 'view', $assinged_task->task->id]) : '' ?></td>
                        <td><?= $assinged_task->has('language') ? $this->Html->link($assinged_task->language->language, ['controller' => 'Languages', 'action' => 'view', $assinged_task->language->id]) : '' ?></td>
                        <td><?= h($assinged_task->task_total_character) ?></td>
                        <td><?= $assinged_task->has('user') ? $this->Html->link($assinged_task->user->name, ['controller' => 'Users', 'action' => 'view', $assinged_task->user->id]) : '' ?></td>
                        <td><?= empty($assinged_task->status) ? 'Inactive' : 'Active' ?></td>
                        <td><?= h($assinged_task->created) ?></td>

                        <?php if ($loggedUser['user_role_id'] == 2): ?>
                            <td><?= $this->Html->link('Go To Task', ['controller' => 'Tasks', 'action' => 'viewAssingedTasksTranslator',$assinged_task->id]) ?></td>
                        <?php else: ?>
                            <td><?= $this->Html->link('View Task', ['controller' => 'Tasks', 'action' => 'viewAssingedTasks',$assinged_task->id]) ?></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>


