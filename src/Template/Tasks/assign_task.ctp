<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?> Task</h1>
            </div>
            <?= $this->Form->create($translator_task,['class'=>'user','type'=>'file','id'=>'assignTaskForm']) ?>
            <div class="form-group">
                <select name="project_id" id="project_id" class="form-control select-control-user">
                    <option value="">Select Project</option>
                    <?php
                    foreach ($projects as $key=>$project){ ?>
                        <option value="<?= $key ?>"><?= $project ?></option>
                    <?php   }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select name="task_id" id="task_id" class="form-control select-control-user">
                    <option value="">Select Task</option>
                </select>
            </div>
            <div class="form-group">
                <input type="hidden" name="source_language" value="" class="form-control form-control-user" id="source_language">
            </div>
            <div class="form-group">
                <input type="text" class="form-control form-control-user" id="source_language_text" placeholder="Source Language" readonly>
            </div>
            <div class="form-group" id="editable_columns"></div>

            <div class="form-group">
                <select name="translated_language" id="translated_language" class="form-control select-control-user">
                    <option value="">Select Translated Language</option>
                    <?php
                    foreach ($languages as $key=>$language){ ?>
                        <option value="<?= $key ?>"><?= $language ?></option>
                    <?php   }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select name="translator_id" id="translator_id" class="form-control select-control-user">
                    <option value="">Select Translator</option>
                    <?php
                    foreach ($users as $key=>$user){ ?>
                        <option value="<?= $key ?>"><?= $user ?></option>
                    <?php   }
                    ?>
                </select>
            </div>
            <button class="btn btn-primary btn-user btn-block" type="button" id="submitAssignTaskForm">Assign Task</button>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>
<script>
    var js_wb_root = "<?= $this->request->getAttribute('webroot') ?>";
        $(document).on('change', '#project_id', function () {
            var project_id = $(this).val();
            $("#task_id").html('<option value="">Select Task</option>');
            $("#source_language").val('');
            $("#source_language_text").val('');
            $("#editable_columns").html('');
            if(project_id !=''){
                getTasks(project_id);
            }
        });
        $(document).on('change', '#task_id', function () {
            var task_id = $(this).val();
            $("#source_language").val('');
            $("#source_language_text").val('');
            $("#editable_columns").html('');
            if(task_id !=''){
                getTaskInfo(task_id);
            }
        });
        $(document).on('click', '#submitAssignTaskForm', function () {
            var project_id = $("#project_id").val();
            if(isEmpty(project_id)){
                toastr.error('Please Choose project.');
                return false;
            }
            var task_id = $("#task_id").val();
            if(isEmpty(task_id)){
                toastr.error('Please Choose Task.');
                return
            }
            var translated_language = $("#translated_language").val();
            if(isEmpty(translated_language)){
                toastr.error('Please Choose Translated Language.');
                return
            }
            var translator_id = $("#translator_id").val();
            if(isEmpty(translator_id)){
                toastr.error('Please Choose Translator.');
                return
            }
            $("#assignTaskForm").submit();
        });

    function getTasks(project_id) {
        $.ajax({
            url: js_wb_root + 'Tasks/getTasks/'+project_id,
            data: {},
            dataType: 'json',
            type: 'GET',
            success: function (res) {
                if (res.status == 'success') {
                    var tasks = '<option value="">Select Task</option>';
                    $.each(res.data,function( index,value ) {
                        tasks+= '<option value="'+index+'">'+value+'</option>';
                    });
                    $("#task_id").html(tasks);

                } else {
                    toastr.error('Please Try again later.');
                }
            }
        });
    }
    function getTaskInfo(task_id) {
        $.ajax({
            url: js_wb_root + 'Tasks/getTaskInfo/'+task_id,
            data: {},
            dataType: 'json',
            type: 'GET',
            success: function (res) {
                if (res.status == 'success') {
                    $("#source_language").val(res.data.language.id);
                    $("#source_language_text").val(res.data.language.language);
                    var columns='';
                    var editable_columns = JSON.parse(res.data.editable_columns);
                    $.each(editable_columns,function( index,value ) {
                        columns+= '<div class="form-check form-check-inline">' +
                            '<input class="form-check-input" name="editable_columns[]" checked type="checkbox"  value="'+index+'">'+value+                            '</div>';
                    });
                    $("#editable_columns").html(columns);

                } else {
                    toastr.error('Please Try again later.');
                }
            }
        });
    }
</script>