    <h3><?= h($task->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Project') ?></th>
            <td><?= $task->has('project') ? $this->Html->link($task->project->title, ['controller' => 'Projects', 'action' => 'view', $task->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File') ?></th>
            <td><?= h(str_replace('files/','',$task->file_path)) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Source Language') ?></th>
            <td><?= $task->has('language') ? $this->Html->link($task->language->language, ['controller' => 'Languages', 'action' => 'view', $task->language->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $task->has('user') ? $this->Html->link($task->user->Name, ['controller' => 'Users', 'action' => 'view', $task->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('All Columns Count') ?></th>
            <td><?= $this->Number->format($task->all_columns_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Editable Columns Count') ?></th>
            <td><?= $this->Number->format($task->editable_columns_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= empty($task->status)?'Inactive':'Active' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($task->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('All Columns') ?></h4>
    </div>
    <div>
        <?= $this->Text->autoParagraph(h(!empty($task->all_columns)?implode(", ",json_decode($task->all_columns,true)):'')); ?>
    </div>
    <div class="row">
        <h4><?= __('Editable Columns') ?></h4>
    </div>
    <div>
        <?= $this->Text->autoParagraph(h(!empty($task->editable_columns)?implode(", ",json_decode($task->editable_columns,true)):'')); ?>
    </div>
