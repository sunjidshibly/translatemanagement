<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Task Data</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col" style="width: 10%">Sl No</th>
                    <th scope="col" style="width: 40%">Source Data</th>
                    <th scope="col" style="width: 40%">Translated Data</th>
                    <th scope="col" style="width: 10%">Total Character</th>
                    <th scope="col" style="width: 10%">Status</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach ($assinged_task_data as $data): ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?php $source_row_data = json_decode($data->source_data, true);
                            foreach ($source_row_data as $source_coloumn_data) {
                                if(empty($source_coloumn_data)){
                                    continue;
                                }
                                echo "<div>$source_coloumn_data</div><hr>";
                            }
                            ?></td>
                        <td><?php $row_data = json_decode($data->translated_data, true);
                            foreach ($row_data as $coloumn_data) {
                                if(empty($coloumn_data)){
                                    continue;
                                }
                                echo "<div>$coloumn_data</div><hr>";
                            }
                            ?></td>
                        <td><?= $data->total_character_count ?></td>
                        <td><?= empty($data->status) ? 'Not Translated' : 'Translated' ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {
        $("#sidebarToggle").click();
    });
</script>


