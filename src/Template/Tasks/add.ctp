<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?> Task</h1>
            </div>
            <?= $this->Form->create($task,['class'=>'user','type'=>'file']) ?>
            <div class="form-group">
                <input type="text" name="title" value="<?= $task->title ?>" class="form-control form-control-user" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <select name="project_id" id="project_id" class="form-control select-control-user">
                    <?php
                    foreach ($projects as $key=>$project){ ?>
                        <option <?= (isset($task->project_id) && $task->project_id == $key)?'selected':'' ?> value="<?= $key ?>"><?= $project ?></option>
                    <?php   }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <select name="source_language" id="source_language" class="form-control select-control-user">
                    <?php
                    foreach ($languages as $key=>$language){ ?>
                        <option <?= (isset($task->source_language) && $task->source_language == $key)?'selected':'' ?> value="<?= $key ?>"><?= $language ?></option>
                    <?php   }
                    ?>
                </select>
            </div>
            <div class="custom-file form-group" style="margin-bottom: 1.5rem !important;">
                <input type="file" name="file" class="custom-file-input form-control-user" id="customFile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            <?php if(!empty($task->all_columns)): ?>
            <div class="form-group">
                <?php
                $columns = json_decode($task->all_columns);
                $editable_columns = isset($task->editable_columns)?array_keys(json_decode($task->editable_columns,true)):[];
                foreach ($columns as $key=>$column):
                ?>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" name="editable_columns[]" <?= in_array($key,$editable_columns)?'checked':'' ?> type="checkbox"  value="<?= $key ?>">
                    <label class="form-check-label" for="inlineCheckbox1"><?= $column ?></label>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <?= $this->Form->button($title.' Task',['class'=>'btn btn-primary btn-user btn-block']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>

<script>
    // following code make the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
</script>