<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?> User!</h1>
            </div>
            <?= $this->Form->create($user,['class'=>'user']) ?>
                <div class="form-group">
                    <?= $this->Form->control('name',['class'=>'form-control form-control-user']); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('email',['class'=>'form-control form-control-user']); ?>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <?= $this->Form->control('password',['class'=>'form-control form-control-user','value'=>'']); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->control('repeat_password',['class'=>'form-control form-control-user']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= $this->Form->label('user_role_id', 'User Role'); ?>
                    <?= $this->Form->select(
                        'user_role_id',
                        $userRoles,
                        [
                            'value' => $user->user_role_id,
                            'class' =>'form-control select-control-user',
                            'level' => 'Select User Role'
                        ]
                    ); ?>
            </div>
            <?= $this->Form->button($title.' Account',['class'=>'btn btn-primary btn-user btn-block']) ?>
            <?= $this->Form->end() ?>
            <hr>
            <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
            </div>
            <div class="text-center">
                <a class="small" href="login.html">Already have an account? Login!</a>
            </div>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>