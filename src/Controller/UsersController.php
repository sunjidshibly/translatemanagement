<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout']);
    }
    public function isAuthorized($user)
    {
        // Check if the user is admin
        if($user['user_role_id'] > 1){
            return false;
        }

        $action = $this->request->getParam('action');
        // The view and index actions are always allowed to logged in admin.
        if (in_array($action, ['view', 'index','add'])) {
            return true;
        }

        // All other actions require a user id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        $modified_user = $this->Users->get($id);

        // Admin can edit Transaltor data
        if($modified_user->user_role_id	> 1){
            return true;
        }

        // Check that the user belongs to the current user.
        return $modified_user->id === $user['id'];
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserRoles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserRoles']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userRoles = $this->Users->UserRoles->find('list',['keyField'=>'id','valueField'=>'role_name'], ['limit' => 200]);
        $title = 'Create';
        $this->set(compact('user', 'userRoles','title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
//            $password = $user->password;
//            $repeat_password = $user->repeat_password;
            $user = $this->Users->patchEntity($user, $this->request->getData());
//            if(empty($this->request->getData('password')) && empty($this->request->getData('repeat-password'))){
//                $user->password = $password;
//                $user->repeat_password = $repeat_password;
//            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userRoles = $this->Users->UserRoles->find('list',['keyField'=>'id','valueField'=>'role_name'], ['limit' => 200]);
        $title = 'Update';
        $this->set(compact('user', 'userRoles', 'title'));
        $this->render('/Users/add');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        $user = $this->Auth->user();
        if($user){
            return $this->redirect('/dashboard');
        }
        $this->viewBuilder()->setLayout('');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
        $this->render('/Pages/home');
    }
    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
}
