<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 *
 * @method \App\Model\Entity\Task[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{
    public $user_id = 0;
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        if (in_array($action, ['view', 'index','assingedTasks','viewAssingedTasksTranslator','resetTaskData','saveTaskData','saveAndNextTaskData'])) {
            if($user['user_role_id'] == 2){

                $this->user_id = $user['id'];
            }
            return true;
        }

        // Check if the user is admin
        return $user['user_role_id'] < 2;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function check_doc_mime($tmpname)
    {
        // MIME types: http://filext.com/faq/office_mime_types.php
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mtype = finfo_file($finfo, $tmpname);
        finfo_close($finfo);
        if ($mtype == ("application/vnd.ms-excel") ||
            $mtype == ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['Projects', 'Languages', 'Users']
        ];
        if(!empty($this->user_id)){
            $translator_tasks_table =TableRegistry::getTableLocator()->get('TranslatorTasks');
            $task_ids = $translator_tasks_table->find('list',['keyField'=>'task_id','valueField'=>'task_id'])->distinct(['task_id'])->where(['translator_id'=>$this->user_id])->toArray();
            $tasks = $this->paginate($this->Tasks->find()->where(['Tasks.id IN'=>$task_ids])->order(['Tasks.id' => 'DESC']));
        } else {
            $tasks = $this->paginate($this->Tasks,['order' => ['id' => 'DESC']]);
        }

        $this->set(compact('tasks'));
    }

    /**
     * View method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $task = $this->Tasks->get($id, [
            'contain' => ['Projects', 'Languages', 'Users']
        ]);

        $this->set('task', $task);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $task = $this->Tasks->newEntity();
        if ($this->request->is('post')) {
            try {
                $file = $this->request->getData('file');
                $task = $this->Tasks->patchEntity($task, $this->request->getData());

                if (!empty($file['tmp_name']) && $this->check_doc_mime($file['tmp_name'])) {

                    $file['name'] = time() . '-' . str_replace(' ', '_', $file['name']);
                    $spreadsheet = IOFactory::load($file['tmp_name']);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                    if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/' . $file['name'])) {
                        $task->file_path = 'files/' . $file['name'];
                        $task->all_columns_count = count($sheetData[1]);
                        $task->all_columns = json_encode($sheetData[1]);
                    }
                }
                $task->created_by = $this->Auth->user()['id'];
                $data = $this->Tasks->save($task);
                if ($data) {
                    $this->Flash->success(__('The task has been saved.'));

                    return $this->redirect(['controller'=>'Tasks','action' => 'edit',$data->id]);
                }
                $this->Flash->error(__('Tasks could not be saved. Please, try again.'));


            } catch (\Exception $ex) {
                $this->Flash->error(__('The task could not be saved. Please, try again.'));
            }
        }
        $projects = $this->Tasks->Projects->find('list', ['limit' => 200]);
        $languages = $this->Tasks->Languages->find('list', ['limit' => 200]);
        $title = 'Create';
        $this->set(compact('task', 'projects', 'languages', 'title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $task = $this->Tasks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $task = $this->Tasks->patchEntity($task, $this->request->getData());
            $editable_fields = $this->request->getData('editable_columns');
            if(!empty($editable_fields)){
                $all_columns = json_decode($task->all_columns,true);
                $editable_field_array=[];
                foreach ($editable_fields as $editable_field){
                    $editable_field_array[$editable_field] = $all_columns[$editable_field];
                }
                $task->editable_columns = json_encode($editable_field_array);
                $task->editable_columns_count = count($editable_fields);
                $task->status = 1;
            }
            $task->created_by = $this->Auth->user()['id'];
            $data = $this->Tasks->save($task);
                if ($data) {
                $this->Flash->success(__('The task has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The task could not be saved. Please, try again.'));
        }
        $projects = $this->Tasks->Projects->find('list', ['limit' => 200]);
        $languages = $this->Tasks->Languages->find('list', ['limit' => 200]);
        $title = 'Update';
        $this->set(compact('task', 'projects', 'languages','title'));
        $this->render('/Tasks/add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $task = $this->Tasks->get($id);
        if ($this->Tasks->delete($task)) {
            $this->Flash->success(__('The task has been deleted.'));
        } else {
            $this->Flash->error(__('The task could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function assignTask()
    {
        $translator_tasks_table = TableRegistry::getTableLocator()->get('TranslatorTasks');
        $translator_task = $translator_tasks_table->newEntity();
        if ($this->request->is('post')) {
            $editable_columns = $this->request->getData('editable_columns');
            $translator_task = $translator_tasks_table->patchEntity($translator_task, $this->request->getData());
            $translator_task->editable_columns = json_encode($editable_columns);
            $translator_task->created_by = $this->Auth->user()['id'];
            $data = $translator_tasks_table->save($translator_task);
            if ($data) {
                $result = $translator_tasks_table->generateTranslatorData($data);
                $this->Flash->success(__('The Task has been assinged.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Task could not be assinged. Please, try again.'));
        }


        $projects = $translator_tasks_table->Projects->find('list', ['limit' => 200]);
        $languages = $translator_tasks_table->Languages->find('list', ['limit' => 200]);
        $users = $translator_tasks_table->Users->find('list', ['limit' => 200])->where(['user_role_id'=>2])->toArray();
        $title = 'Assign';
        $this->set(compact('translator_task', 'projects', 'languages', 'title','users'));

    }

    public function getTasks($project_id)
    {
        $tasks = $this->Tasks->getTasksByProject($project_id);
        if (!empty($tasks)) {
            return $this->jsonResponse($this->responseFormat('success', $tasks));
        } else {
            return $this->jsonResponse($this->responseFormat('error', 'Data not found.'));
        }
    }

    public function getTaskInfo($task_id)
    {
        $task_info = $this->Tasks->getTaskInfo($task_id);
        if (!empty($task_info)) {
            return $this->jsonResponse($this->responseFormat('success', $task_info));
        } else {
            return $this->jsonResponse($this->responseFormat('error', 'Data not found.'));
        }
    }

    public function assingedTasks()
    {
        $translator_tasks_table = TableRegistry::getTableLocator()->get('TranslatorTasks');
        $translator_tasks_table->paginate = [
            'contain' => ['Projects', 'Tasks', 'Languages', 'Users']
        ];

        if(!empty($this->user_id)){
            $assinged_tasks = $this->paginate($translator_tasks_table->index($this->user_id),['order' => ['id' => 'DESC']]);
        } else {
            $assinged_tasks = $this->paginate($translator_tasks_table->index(),['order' => ['id' => 'DESC']]);
        }
        $this->set(compact('assinged_tasks'));
    }

    public function viewAssingedTasks($translator_task_id)
    {
        $task_datas_table = TableRegistry::getTableLocator()->get('task_datas');
        $assinged_task_data = $this->paginate($task_datas_table->find()->where(['translator_task_id' => $translator_task_id]));
        $this->set(compact('assinged_task_data'));
    }

    public function viewAssingedTasksTranslator($translator_task_id)
    {
        $task_datas_table = TableRegistry::getTableLocator()->get('task_datas');
        $assinged_task_data = $this->paginate($task_datas_table->find()->where(['translator_task_id' => $translator_task_id, 'status' => 0]), ['limit' => 1]);
        $this->set(compact('assinged_task_data'));
    }

    public function resetTaskData($task_data_id)
    {
        $task_datas_table = TableRegistry::getTableLocator()->get('task_datas');
        $task_data = $task_datas_table->get($task_data_id);

        if ($this->checkTranslatorOwnData($task_data->translator_id, $this->user_id)) {
            $task_data->translated_data = $task_data->source_data;
            if ($task_datas_table->save($task_data)) {
                $this->Flash->success(__('Data has been reset.'));
                return $this->redirect($this->referer());
            }
        }
        $this->Flash->error(__('Data could not be reset. Please, try again.'));
        return $this->redirect(['action' => 'index']);

    }

    public function saveTaskData($task_data_id)
    {
        if($this->request->is('post')) {
            $task_datas_table = TableRegistry::getTableLocator()->get('task_datas');
            $task_data = $task_datas_table->get($task_data_id);

            if ($this->checkTranslatorOwnData($task_data->translator_id, $this->user_id)) {
                $translated_data = $this->request->getData('translated_data');
                if (!empty($translated_data)) {
                    $task_data->translated_data = $translated_data;
                    if ($task_datas_table->save($task_data)) {
                        $this->Flash->success(__('Data has been saved.'));
                        return $this->redirect($this->referer());
                    }
                }
            }
        }
        $this->Flash->error(__('Data could not be saved. Please, try again.'));
        return $this->redirect(['action' => 'index']);
    }

    public function saveAndNextTaskData($task_data_id)
    {
        if($this->request->is('post')) {
            $task_datas_table = TableRegistry::getTableLocator()->get('task_datas');
            $task_data = $task_datas_table->get($task_data_id);

            if ($this->checkTranslatorOwnData($task_data->translator_id, $this->user_id)) {
                $translated_data = $this->request->getData('translated_data');
                if (!empty($translated_data)) {
                    $task_data->translated_data = $translated_data;
                    $task_data->status = 1;
                    if ($task_datas_table->save($task_data)) {
                        $this->Flash->success(__('Data has been saved.'));
                        return $this->redirect(['action' => 'viewAssingedTasksTranslator',$task_data->translator_task_id]);
                    }
                }
            }
        }
        $this->Flash->error(__('Data could not be saved. Please, try again.'));
        return $this->redirect(['action' => 'index']);
    }

    protected function checkTranslatorOwnData($task_data_user_id, $user_id)
    {
        return ($task_data_user_id == $user_id);
    }
}
