<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => array(
                'controller' => 'Dashboard',
                'action' => 'dashboard'
            ),            // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);

        // Allow the display action so our PagesController
        // $this->Auth->allow(['display', 'view', 'index']);
        $this->Auth->allow(['display']);
    }
    public function beforeFilter(Event $event)
    {
        $loggedUser = $this->Auth->user();
        $this->set('loggedUser', $loggedUser);
    }
    public function isAuthorized($user)
    {
        // By default deny access.
        return false;
    }
    protected function responseFormat($status = 'error', $data = '',$options = []){
        $response = [''];
        if(!empty($status)){
            if($status == 'success'){
                $response = [
                    'status' => $status,
                    'data' => $data
                ];
            }
            elseif ($status == 'error'){
                $response = [
                    'status' => $status,
                    'message' => $data
                ];
                if(!empty($options) && $options['details']){
                    $response['details'] = $options['details'];
                }
                if(!empty($options) && $options['code']){
                    $response['code'] = $options['code'];
                }
            }
        }
        return $response;
    }
    public function jsonResponse($body,$code=''){
        $response = $this->response->withType("application/json")->withStringBody(json_encode($body));
        if(!empty($code)){
            $response = $response->withStatus($code);
        }
        return $response;
    }
}
