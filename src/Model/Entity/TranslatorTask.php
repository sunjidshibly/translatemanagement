<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TranslatorTask Entity
 *
 * @property int $id
 * @property string $editable_columns
 * @property int $language_id
 * @property int $translated_language
 * @property int $translator_id
 * @property int $status
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\Translator $translator
 * @property \App\Model\Entity\User $user
 */
class TranslatorTask extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'editable_columns' => true,
        'source_language' => true,
        'translated_language' => true,
        'translator_id' => true,
        'task_id'=>true,
        'project_id'=>true,
        'status' => true,
        'created_by' => true,
        'created' => true,
        'modified' => true,
        'language' => true,
        'translator' => true,
        'user' => true,
        'task'=>true,
        'project'=>true,
        'task_total_character'=>true
    ];
}
