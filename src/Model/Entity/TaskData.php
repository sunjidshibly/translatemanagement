<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TaskData Entity
 *
 * @property int $id
 * @property int $translator_task_id
 * @property int $row_serial
 * @property string $source_data
 * @property string $translated_data
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_id
 *
 * @property \App\Model\Entity\TranslatorTask $translator_task
 * @property \App\Model\Entity\User $user
 */
class TaskData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'translator_task_id' => true,
        'row_serial' => true,
        'source_data' => true,
        'translated_data' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'translator_id' => true,
        'translator_task' => true,
        'total_character_count' => true,
        'user' => true
    ];
}
