<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity
 *
 * @property int $id
 * @property string $title
 * @property int $project_id
 * @property string|null $file_path
 * @property int $language_id
 * @property string|null $all_columns
 * @property int $all_columns_count
 * @property string|null $editable_columns
 * @property int $editable_columns_count
 * @property int $status
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\User $user
 */
class Task extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'project_id' => true,
        'file_path' => true,
        'source_language' => true,
        'all_columns' => true,
        'all_columns_count' => true,
        'editable_columns' => true,
        'editable_columns_count' => true,
        'status' => true,
        'created_by' => true,
        'created' => true,
        'modified' => true,
        'project' => true,
        'language' => true,
        'user' => true
    ];
}
