<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaskDatas Model
 *
 * @property \App\Model\Table\TranslatorTasksTable&\Cake\ORM\Association\BelongsTo $TranslatorTasks
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\TaskData get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaskData newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaskData[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaskData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaskData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaskData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaskData[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaskData findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TaskDatasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('task_datas');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('TranslatorTasks', [
            'foreignKey' => 'translator_task_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'translator_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->integer('row_serial')
            ->requirePresence('row_serial', 'create')
            ->notEmptyString('row_serial');

        $validator
            ->scalar('source_data')
            ->requirePresence('source_data', 'create')
            ->notEmptyString('source_data');

        $validator
            ->scalar('total_character_count');
        $validator
            ->scalar('translated_data')
            ->requirePresence('translated_data', 'create')
            ->notEmptyString('translated_data');

        $validator
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['translator_task_id'], 'TranslatorTasks'));
        $rules->add($rules->existsIn(['translator_id'], 'Users'));

        return $rules;
    }
}
