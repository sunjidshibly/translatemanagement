<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * TranslatorTasks Model
 *
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\BelongsTo $Languages
 * @property \App\Model\Table\TranslatorsTable&\Cake\ORM\Association\BelongsTo $Translators
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\TranslatorTask get($primaryKey, $options = [])
 * @method \App\Model\Entity\TranslatorTask newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TranslatorTask[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TranslatorTask|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TranslatorTask saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TranslatorTask patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TranslatorTask[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TranslatorTask findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TranslatorTasksTable extends Table
{
    protected $unset_property = ['modified','created_by','source_language'];
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('translator_tasks');
        $this->setDisplayField('task_id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Languages', [
            'foreignKey' => 'source_language',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Languages', [
            'foreignKey' => 'translated_language',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'created_by',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'translator_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tasks', [
            'foreignKey' => 'task_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('editable_columns')
            ->requirePresence('editable_columns', 'create')
            ->notEmptyString('editable_columns');

        $validator
            ->scalar('task_total_character');

        $validator
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['source_language'], 'Languages'));
        $rules->add($rules->existsIn(['translated_language'], 'Languages'));
        $rules->add($rules->existsIn(['translator_id'], 'Users'));
        $rules->add($rules->existsIn(['created_by'], 'Users'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        $rules->add($rules->existsIn(['task_id'], 'Tasks'));

        return $rules;
    }
    public function index($user_id = 0){
        $query = $this->find()->selectAllExcept($this,$this->unset_property)
            ->contain([
                'Users' => [
                    'fields' => [
                        'Users.id',
                        'Users.name',
                    ]
                ]
            ])
            ->contain([
                'Projects' => [
                    'fields' => [
                        'Projects.id',
                        'Projects.title',
                    ]
                ]
            ])
            ->contain([
                'Tasks' => [
                    'fields' => [
                        'Tasks.id',
                        'Tasks.title',
                    ]
                ]
            ])
            ->contain([
                'Languages' => [
                    'fields' => [
                        'Languages.id',
                        'Languages.language',
                    ]
                ]
            ]);
        if(!empty($user_id)){
            $query = $query->where(['TranslatorTasks.translator_id'=>$user_id]);
        }
        return $query;
    }
    public function generateTranslatorData($translator_task){
        $tasks_table = TableRegistry::getTableLocator()->get('Tasks');
        $task_datas_table = TableRegistry::getTableLocator()->get('TaskDatas');
        try {
            $task_info = $tasks_table->get($translator_task->task_id);
            $spreadsheet = IOFactory::load(WWW_ROOT . DIRECTORY_SEPARATOR . $task_info->file_path);
            $sheetDatas = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $editable_columns = json_decode($translator_task->editable_columns, true);
            $i = 0;
            $task_character_count = 0;
            foreach ($sheetDatas as $rowId => $sheetData) {
                $i++;
                if ($i == 1) {
                    continue;
                }
                $eachRowData = [];
                $total_character_count = 0;
                foreach ($sheetData as $key => $eachRowDataByColoumn) {
                    if (in_array($key, $editable_columns)) {
                        $eachRowData[$key] = $eachRowDataByColoumn;
                        $trim = strip_tags($eachRowDataByColoumn);
                        $trim=str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","{","}","[","]","&nbsp;"], '', $trim);
                        $trim = preg_replace("/[$&+,:;=?@#|'<>.^*()%!-]/", '', $trim);
                        $trim = preg_replace("/[\"\']/", '', $trim);

                        $character_count = strlen(utf8_decode($trim));
                        $total_character_count = $total_character_count + $character_count;
                    }

                }
                $each_row_entity = $task_datas_table->newEntity();
                $each_row_entity->translator_task_id = $translator_task->id;
                $each_row_entity->row_serial = $rowId;
                $each_row_entity->source_data = json_encode($eachRowData);
                $each_row_entity->translated_data = json_encode($eachRowData);
                $each_row_entity->total_character_count = $total_character_count;
                $each_row_entity->status = 0;
                $each_row_entity->translator_id = $translator_task->translator_id;
                $task_character_count = $task_character_count + $total_character_count;
                $task_datas_table->save($each_row_entity);
            }
            $translator_task = $this->get($translator_task->id);
            $translator_task->task_total_character = $task_character_count;
            $this->save($translator_task);
            return ['status'=>'success'];
        }catch (\Exception $ex){
            return ['status'=>'error','message'=>$ex->getMessage()];
        }
    }
}
